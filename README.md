Android Native + SDL2 + Lua Example
Copyright (c) 2013 Martin Felis <martin@fysx.org>

This is a simple example that integrates SDL2 and Lua in a Android
App that uses the Android NDK.

Instructions:
-------------

Install the Android SDK and Android NDK and run

    ndk-build

in the root folder of this project.

Features:
---------
* loading and running of a Lua script
* modification of the color using Lua
* reacting on a touch event using Lua code
* calling a lua function on every frame that does stuff

License:
--------

This code is public domain, so feel free to do whatever you want with it.
No liability however.
